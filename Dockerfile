# build stage
FROM node:12-alpine as build-stage

LABEL maintainer=275284429@qq.com

# 创建一个工作目录
WORKDIR /vuemobiletemplate

COPY . .

RUN npm install cnpm -g --no-progress --registry=https://registry.npm.taobao.org

RUN cnpm install --no-progress

RUN npm run build

# production stage
FROM nginx:stable-alpine as production-stage

COPY --from=build-stage /vuemobiletemplate/dist /usr/share/nginx/html

EXPOSE 11012

CMD ["nginx", "-g", "daemon off;"]
